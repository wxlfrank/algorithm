package org.learn.wxlfrank;

import org.junit.jupiter.api.Test;
import org.learn.wxlfrank.CutPodSolution.AuxData;

import java.util.*;

public class CutPodSolutionTest {
    @Test
    public void testCutPod() {
        Map<Integer, Integer> prices = new HashMap<Integer, Integer>();
        prices.put(1, 1);
        prices.put(2, 5);
        prices.put(3, 8);
        prices.put(4, 9);
        prices.put(5, 10);
        prices.put(6, 17);
        CutPodSolution pod = new CutPodSolution();
        AuxData result = pod.findMaxPods(prices, 4);
        System.out.println(result);
    }

}
