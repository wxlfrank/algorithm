package org.learn.wxlfrank;

public class Matrix {
    int rows;
    int columns;
    int[][] data;

    public Matrix(int rows, int columns, int[][] data) {
        this.rows = rows;
        this.columns = columns;
        this.data = data;
    }

    public Matrix(int rows2, int columns2) {
        this.rows = rows2;
        this.columns = columns2;
        this.data = new int[rows2][columns2];
    }

    public int getValue(int row, int column) {
        return data[row][column];
    }

    public Matrix multiply(Matrix a, Matrix b) {
        Matrix c = new Matrix(a.rows, b.columns);
        for (int r = 0; r < c.rows; r++) {
            for (int o = 0; o < c.columns; o++) {
                int cro = 0;
                for (int i = 0; i < a.columns; i++) {
                    cro += a.getValue(r, i) * b.getValue(i, o);
                }
                c.setValue(r, o, cro);
            }
        }
        return c;
    }

    private void setValue(int r, int o, int cro) {
        data[r][o] = cro;
    }
}
