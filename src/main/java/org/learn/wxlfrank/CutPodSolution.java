package org.learn.wxlfrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CutPodSolution {
    static class AuxData {
        public AuxData(int weight, List<Integer> solution) {
            this.weight = weight;
            this.solution = solution;
        }

        public AuxData() {
            weight = 0;
            solution = new ArrayList<Integer>();
        }

        int weight;
        List<Integer> solution;

        public String toString() {
            return "" + weight + ": "
                    + String.join(", ", solution.stream().map(i -> i.toString()).collect(Collectors.toList()));
        }
    }

    public AuxData findMaxPods(Map<Integer, Integer> prices, int length) {
        List<Integer> sizes = new ArrayList<Integer>(prices.keySet());
        sizes.sort(Integer::compare);
        return findBestSolution(length, sizes.toArray(new Integer[sizes.size()]), prices,
                new HashMap<Integer, AuxData>());

    }

    private AuxData findBestSolution(int left, Integer[] sizes,
            final Map<Integer, Integer> unitWeight, Map<Integer, AuxData> hash) {
        if (left < sizes[0]) {
            return hash.computeIfAbsent(left, i -> new AuxData());

        }
        if (hash.containsKey(left)) {
            return hash.get(left);
        }
        AuxData best = new AuxData();
        AuxData bestLeft = null;
        best.solution.add(0);
        AuxData curLeft;
        for (int size : sizes) {
            if (size > left)
                break;
            curLeft = hash.computeIfAbsent(left - size, i -> findBestSolution(left - size, sizes, unitWeight, hash));
            int curWeight = curLeft.weight + unitWeight.get(size);
            if (curWeight > best.weight) {
                best.weight = curWeight;
                best.solution.set(0, size);
                bestLeft = curLeft;
            }
        }
        best.solution.addAll(bestLeft.solution);
        hash.put(left, best);
        return best;
    }
}
